﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace PJGame
{
    public partial class DBGameContext : DbContext
    {
        public DBGameContext()
        {
        }

        public DBGameContext(DbContextOptions<DBGameContext> options) : base(options)
        {
        }

        public virtual DbSet<TbgameHistory> TbgameHistories { get; set; } = null!;
        public virtual DbSet<Vwstatistic> Vwstatistics { get; set; } = null!;

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer("Server=.;Database=DBGame;User Id=sa;Password=Abcd1234!");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<TbgameHistory>(entity =>
            {
                entity.HasKey(e => e.GameId);

                entity.ToTable("TBGameHistory");

                entity.Property(e => e.GameId).HasColumnName("GameID");

                entity.Property(e => e.GameStart).HasColumnType("datetime");

                entity.Property(e => e.GameStatusDisplay).HasMaxLength(50);

                entity.Property(e => e.GameStop).HasColumnType("datetime");
            });

            modelBuilder.Entity<Vwstatistic>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("VWStatistics");

                entity.Property(e => e.GameCountAll).HasColumnType("numeric(13, 1)");

                entity.Property(e => e.GameStatusDisplay).HasMaxLength(50);

                entity.Property(e => e.Result).HasColumnType("numeric(29, 14)");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
