﻿using System;
using System.Collections.Generic;

namespace PJGame
{
    public partial class TbgameHistory
    {
        public int GameId { get; set; }
        public DateTime? GameStart { get; set; }
        public DateTime? GameStop { get; set; }
        public int? GameDuration { get; set; }
        public string? GameStatusDisplay { get; set; }
    }
}
