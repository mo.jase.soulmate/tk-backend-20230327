﻿using System;
using System.Collections.Generic;

namespace PJGame
{
    public partial class Vwstatistic
    {
        public string? GameStatusDisplay { get; set; }
        public int? GameCountByStatus { get; set; }
        public decimal? GameCountAll { get; set; }
        public decimal? Result { get; set; }
    }
}
