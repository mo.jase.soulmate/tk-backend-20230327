USE [DBGame]
GO
/****** Object:  Table [dbo].[TBGameHistory]    Script Date: 3/28/2023 9:05:44 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TBGameHistory](
	[GameID] [int] IDENTITY(1,1) NOT NULL,
	[GameStart] [datetime] NULL,
	[GameStop] [datetime] NULL,
	[GameDuration] [int] NULL,
	[GameStatusDisplay] [nvarchar](50) NULL,
 CONSTRAINT [PK_TBGameHistory] PRIMARY KEY CLUSTERED 
(
	[GameID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[VWStatistics]    Script Date: 3/28/2023 9:05:44 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[VWStatistics] AS
SELECT TB1.GameStatusDisplay,TB1.GameCountByStatus,TB2.GameCountAll,((TB1.GameCountByStatus/TB2.GameCountAll)*100) As Result FROM
(
  SELECT GameStatusDisplay,COUNT(GameID) As GameCountByStatus
  FROM [DBGame].[dbo].[TBGameHistory]
  GROUP BY GameStatusDisplay
) as TB1
LEFT JOIN (
  SELECT (COUNT(*)*1.0) As GameCountAll
  FROM [DBGame].[dbo].[TBGameHistory]
) as TB2
ON TB1.GameCountByStatus!=TB2.GameCountAll

GO
SET IDENTITY_INSERT [dbo].[TBGameHistory] ON 

INSERT [dbo].[TBGameHistory] ([GameID], [GameStart], [GameStop], [GameDuration], [GameStatusDisplay]) VALUES (1, CAST(N'2023-01-01T00:00:00.000' AS DateTime), CAST(N'2023-01-01T00:00:00.000' AS DateTime), 20, N'OWIN')
INSERT [dbo].[TBGameHistory] ([GameID], [GameStart], [GameStop], [GameDuration], [GameStatusDisplay]) VALUES (2, CAST(N'2023-03-27T10:09:45.567' AS DateTime), CAST(N'2023-03-27T10:19:03.457' AS DateTime), 554, N'OWIN')
INSERT [dbo].[TBGameHistory] ([GameID], [GameStart], [GameStop], [GameDuration], [GameStatusDisplay]) VALUES (3, CAST(N'2023-03-27T11:20:00.000' AS DateTime), CAST(N'2023-03-27T11:20:05.000' AS DateTime), 20, N'XWIN')
INSERT [dbo].[TBGameHistory] ([GameID], [GameStart], [GameStop], [GameDuration], [GameStatusDisplay]) VALUES (4, CAST(N'2023-03-27T11:20:00.000' AS DateTime), CAST(N'2023-03-27T11:20:05.000' AS DateTime), 20, N'XWIN')
INSERT [dbo].[TBGameHistory] ([GameID], [GameStart], [GameStop], [GameDuration], [GameStatusDisplay]) VALUES (5, CAST(N'2023-03-27T11:20:00.000' AS DateTime), CAST(N'2023-03-27T11:20:05.000' AS DateTime), 20, N'XWIN')
INSERT [dbo].[TBGameHistory] ([GameID], [GameStart], [GameStop], [GameDuration], [GameStatusDisplay]) VALUES (6, CAST(N'2023-03-27T11:20:00.000' AS DateTime), CAST(N'2023-03-27T11:20:05.000' AS DateTime), 20, N'XWIN')
INSERT [dbo].[TBGameHistory] ([GameID], [GameStart], [GameStop], [GameDuration], [GameStatusDisplay]) VALUES (7, CAST(N'2023-03-27T11:20:00.000' AS DateTime), CAST(N'2023-03-27T11:20:05.000' AS DateTime), 20, N'QUIT')
INSERT [dbo].[TBGameHistory] ([GameID], [GameStart], [GameStop], [GameDuration], [GameStatusDisplay]) VALUES (8, CAST(N'2023-03-27T11:20:00.000' AS DateTime), CAST(N'2023-03-27T11:20:05.000' AS DateTime), 20, N'QUIT')
INSERT [dbo].[TBGameHistory] ([GameID], [GameStart], [GameStop], [GameDuration], [GameStatusDisplay]) VALUES (9, CAST(N'2023-03-27T11:20:00.000' AS DateTime), CAST(N'2023-03-27T11:20:05.000' AS DateTime), 20, N'QUIT')
INSERT [dbo].[TBGameHistory] ([GameID], [GameStart], [GameStop], [GameDuration], [GameStatusDisplay]) VALUES (10, CAST(N'2023-03-27T11:20:00.000' AS DateTime), CAST(N'2023-03-27T11:20:05.000' AS DateTime), 20, N'DRAW')
SET IDENTITY_INSERT [dbo].[TBGameHistory] OFF
GO
