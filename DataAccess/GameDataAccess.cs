﻿using Microsoft.EntityFrameworkCore;
using PJGame.DataAccess;

namespace PJGame.DataAccess
{
    public class GameDataAccess : IGameDataAccess
    {
        public readonly DBGameContext _context;
        public GameDataAccess(DBGameContext context)
        {
            _context = context;

        }


        public List<TbgameHistory> GetAll()
        {
            var test = new List<TbgameHistory>();
            try
            {
                test = _context.TbgameHistories.ToList();
                return test;
            }
            catch (Exception ex)
            {
                return test;
            }
        }

        //List<Vwstatistic> GetStatistic()
        public List<Vwstatistic> GetStatistic()
        {
            var test = new List<Vwstatistic>();
            try
            {
                test = _context.Vwstatistics.ToList();
                return test;
            }
            catch (Exception ex)
            {
                return test;
            }
        }

        public TbgameHistory SaveGameHistory(TbgameHistory model)
        {

            DateTime DateNow = DateTime.Now;
            bool IsEdit = false;
            var game = _context.TbgameHistories.SingleOrDefault(x => x.GameId == model.GameId);
            if (game == null)
            {
                game = new TbgameHistory
                {
                    GameStart = DateNow
                };
            }
            else
                IsEdit = false;


            if (IsEdit == true)
            {
                _context.TbgameHistories.Add(game);
            }
            else
            {
                game.GameDuration = Convert.ToInt32((DateNow - game.GameStart.Value).TotalSeconds);
                game.GameStop = DateTime.Now;
                game.GameStatusDisplay = model.GameStatusDisplay;
                _context.Entry(game).State = EntityState.Modified;
            }
            _context.SaveChanges();
            return game;
        }

    }
}
