﻿namespace PJGame.DataAccess
{
    public interface IGameDataAccess
    {
        List<TbgameHistory> GetAll();
        List<Vwstatistic> GetStatistic();
        TbgameHistory SaveGameHistory(TbgameHistory model);
    }
}
