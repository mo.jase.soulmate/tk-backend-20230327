﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PJGame.DataAccess;

namespace PJGame.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class GameHistoryController : ControllerBase
    {
        //Scaffold-DbContext "Server=.;Database=DBGame;User Id=sa;Password=Abcd1234!" Microsoft.EntityFrameworkCore.SqlServer -OutputDir DbContext
        private readonly IGameDataAccess gameDataAccess;
        public GameHistoryController(DBGameContext dbContext)
        {
            gameDataAccess = new GameDataAccess(dbContext);
        }

       
        [HttpPost]
        [Route("GetAll")]
        public List<TbgameHistory> GetAll()
        {
            var game = gameDataAccess.GetAll();
            return game;
        }

        //Save Result For Frontend
        [HttpPost]
        [Route("SaveGameHistory")]
        public TbgameHistory SaveGameHistory(TbgameHistory model)
        {
            var game = gameDataAccess.SaveGameHistory(model);
            return game;
        }


        [HttpPost]
        [Route("GetStatistic")]
        public List<Vwstatistic> GetStatistic()
        {
            var game = gameDataAccess.GetStatistic();
            return game;
        }



      
    }
}
